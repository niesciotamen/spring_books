package com.nesciotamen.spring_books.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Author {

    private Long id;
    private String name;
    private String surname;
    private int yearOfBirth;
    private String placeOfBirth;
    private Set<Book> books;
}
