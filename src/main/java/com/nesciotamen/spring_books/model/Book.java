package com.nesciotamen.spring_books.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Book {

    private Long id;

    private String title;
    private int yearPublished;
    private BookType bookType;
    private int pages;
    private Author author;

}
