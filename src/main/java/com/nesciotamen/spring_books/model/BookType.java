package com.nesciotamen.spring_books.model;

public enum BookType {
    ADVENTURES,
    ACTION,
    SCIENCE;
}
